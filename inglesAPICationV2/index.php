<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Ingles API-CATION</title>
  <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href="./style.css">

</head>
<body>

<?php

    assert_options(ASSERT_ACTIVE, 1);
    assert_options(ASSERT_WARNING, 0);
    assert_options(ASSERT_BAIL, 0);
    assert_options(ASSERT_QUIET_EVAL, 1);

    function my_assert_handler($file, $line, $code, $desc = null)
    {
        //echo "$file $line $code";
        echo "<script>console.log('Assertion failed at".$file.":".$line.":" .$code."');</script>";
        if ($desc) {
            echo "$desc";
        }
        echo "\n";
    }
    assert_options(ASSERT_CALLBACK,'my_assert_handler');

	$wordErr = "";
    $word  = "";

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (empty($_POST["word"])) {
		$wordErr = "Word is required";
		assert($wordErr == "Word is required", "<script>console.log( 'No captura el textbox' );</script>" );
	  } else {
		$word = test_input($_POST["word"]);
		try{
			checkWord($word);
		}
	
		catch(Exception $e){
			$word = clean($word);
			$wordErr = 'Message: '.$e->getMessage();
		}
	  }
	}

    function clean($string)
	{
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
	}

	function checkWord($data){
		if (!preg_match("/^[a-zA-Z ]*$/",$data)) { 
		
		  throw new Exception("Only letters and white space allowed");
		}
		
		return true;
    }
    
	function test_input($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}
?>


<div id="aspect-content">
        <h1>&nbsp Inglés API-CATION</h1>
        <p>&nbsp &nbsp ¡Traduce la palabra que quieras y adquiere ejemplos!</p> 
        <form class= "w3-col" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
            <div class="w3-row-padding">
                <div class="w3-half">
                    <input class="w3-input w3-border w3-round" name="word" value="<?php echo $word;?>" type="text" placeholder="INGRESE UNA PALABRA"></p>
                    <input class="button button3" type="submit" name="submit" value="BUSCAR" > 	 
                    <span class="error">* <?php echo $wordErr;?></span>
                </div>
            </div>
        </form>
</div>

<?php
	$url = 'https://linguatools-conjugations.p.rapidapi.com/conjugate/';
	$request_url = $url . '?verb=' . $word;

	$curl = curl_init($request_url);

	curl_setopt_array($curl, array(CURLOPT_CUSTOMREQUEST =>'GET'));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, [
	  'X-RapidAPI-Host: linguatools-conjugations.p.rapidapi.com',
	  'X-RapidAPI-Key: dd9b30ac44msh1963fe8a53d6448p1889d2jsnafe1d3641ad1',
	  'Content-Type: application/json'
	]);

	curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	
	$response = curl_exec($curl);

	$respuesta = json_decode($response);
	curl_close($curl);
?> 


<div id="aspect-content">
    <?php echo "<h2>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Word: $word</h2>"?> 

<div class="aspect-tab ">
    <input id="item-01" type="checkbox" class="aspect-input" name="aspect">
    <label for="item-01" class="aspect-label"></label>
    <div class="aspect-content">
        <div class="aspect-info">
            <span class="aspect-name">Google Translate</span>
        </div>
    </div>
    <div class="aspect-tab-content">
        <div class="sentiment-wrapper ">
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Traducción</span>
                    </div>
                    <div>
                    <?php 			
                        include "action_google_translate.php";
                        googleTrans($word);
                    ?>
                    </div>
                </div>
            </div>
            
        </div> 
        
    </div>
    </div>
    <?php echo "<h2>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Only Verbs</h2>"?> 
  <div class="aspect-tab ">
    <input id="item-02" type="checkbox" class="aspect-input" name="aspect">
    <label for="item-02" class="aspect-label"></label>
    <div class="aspect-content">
        <div class="aspect-info">
            <span class="aspect-name">Conjugated</span>
        </div>
    </div>
    <div class="aspect-tab-content">
        <div class="sentiment-wrapper ">
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Conjugated Forms:</span>
                    </div>
                    <div>
                    <?php 			
                        include "action_rapid_api.php";
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        conjugations($word,$respuesta);
                    ?>
                    </div>
                </div>
            </div>
            
        </div> 
        
    </div>
    

  </div>
  <div class="aspect-tab ">
    <input id="item-03" type="checkbox" class="aspect-input" name="aspect">
    <label for="item-03" class="aspect-label"></label>
    <div class="aspect-content">
        <div class="aspect-info">
            <span class="aspect-name">Conditionals</span>
        </div>
    </div>
    <div class="aspect-tab-content">
        <div class="sentiment-wrapper ">
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Conditionals Present</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        conditionals($word,$respuesta,0);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Conditionals Present Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        conditionals($word,$respuesta,1);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Conditionals Perfect</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        conditionals($word,$respuesta,2);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Conditionals Perfect Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        conditionals($word,$respuesta,3);
                    ?>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
  </div>
  <div class="aspect-tab ">
    <input id="item-04" type="checkbox" class="aspect-input" name="aspect">
    <label for="item-04" class="aspect-label"></label>
    <div class="aspect-content">
        <div class="aspect-info">
            <span class="aspect-name">Indicatives</span>
        </div>
    </div>
    <div class="aspect-tab-content">
        <div class="sentiment-wrapper ">
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Simple Present</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        indicatives($word,$respuesta,0);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Present Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        indicatives($word,$respuesta,1);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Present Perfect</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        indicatives($word,$respuesta,2);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Present Perfect Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        indicatives($word,$respuesta,3);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Simple Past</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        indicatives($word,$respuesta,4);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Past Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        indicatives($word,$respuesta,5);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Past Perfect</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        indicatives($word,$respuesta,6);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Past Perfect Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        indicatives($word,$respuesta,7);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Simple Future</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        indicatives($word,$respuesta,8);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Future Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        indicatives($word,$respuesta,9);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Future Perfect</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        indicatives($word,$respuesta,10);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Future Perfect Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        indicatives($word,$respuesta,11);
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <div class="aspect-tab ">
    <input id="item-05" type="checkbox" class="aspect-input" name="aspect">
    <label for="item-05" class="aspect-label"></label>
    <div class="aspect-content">
        <div class="aspect-info">
            <span class="aspect-name">Passives</span>
        </div>
    </div>
    <div class="aspect-tab-content">
        <div class="sentiment-wrapper ">
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Passive Simple Present</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        passives($word,$respuesta,0);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Present Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        passives($word,$respuesta,1);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Present Perfect</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        passives($word,$respuesta,2);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Present Perfect Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        passives($word,$respuesta,3);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Simple Past</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        passives($word,$respuesta,4);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Past Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        passives($word,$respuesta,5);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Past Perfect</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        passives($word,$respuesta,6);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Past Perfect Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        passives($word,$respuesta,7);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Simple Future</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        passives($word,$respuesta,8);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Future Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        passives($word,$respuesta,9);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Future Perfect</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        passives($word,$respuesta,10);
                    ?>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="positive-count opinion-header">
                        <span>Future Perfect Progressive</span>
                    </div>
                    <div>
                    <?php 			
                        if(array_key_exists('message',$respuesta))
                        {
                            assert($respuesta->message != "Oops, an error occurred. Please contact support@rapidapi.com","<script>console.log( 'Revise datos del header' );</script>");
                        }
                        passives($word,$respuesta,11);
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  
</div>
<!-- partial -->
  
</body>
</html>
