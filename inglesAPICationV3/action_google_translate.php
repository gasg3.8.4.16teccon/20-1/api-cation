<?php


function googleTrans($word)
{
	
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://google-translate1.p.rapidapi.com/language/translate/v2",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "source=en&q=".$word."&target=es",
		CURLOPT_HTTPHEADER => array(
			"accept-encoding: application/gzip",
			"content-type: application/x-www-form-urlencoded",
			"x-rapidapi-host: google-translate1.p.rapidapi.com",
			"x-rapidapi-key: 797e09afc8msh4b5628dcea4509cp168248jsn0aebeaf84a5a"
		),
	));

	curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	$response = curl_exec($curl);
	$err = curl_error($curl);
	$respuesta = json_decode($response);
	curl_close($curl);
	
	
	$traduccion = $respuesta->data->translations[0]->translatedText;
	
	//echo($traduccion);
	
	
	echo "<h4> $traduccion </h4>";

}
?>