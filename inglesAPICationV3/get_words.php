<?php


assert_options(ASSERT_ACTIVE, 1);
assert_options(ASSERT_WARNING, 0);
assert_options(ASSERT_BAIL, 0);
assert_options(ASSERT_QUIET_EVAL, 1);

function my_assert_handler($file, $line, $code, $desc = null)
{
    //echo "$file $line $code";
    echo "<script>console.log('Assertion failed at".$file.":".$line.":" .$code."');</script>";
    if ($desc) {
        echo "$desc";
    }
    echo "\n";
}
assert_options(ASSERT_CALLBACK,'my_assert_handler');

$wordErr = "";
$word  = "";

if(isset($_GET["word"]) )
{
    $word = $_GET["word"];
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["word"])) {
    $wordErr = "Word is required";
    assert($wordErr == "Word is required", "<script>console.log( 'No captura el textbox' );</script>" );
  } else {
    $word = test_input($_POST["word"]);
    try{
        checkWord($word);
    }

    catch(Exception $e){
        $word = clean($word);
        $wordErr = 'Message: '.$e->getMessage();
    }
  }
}

function clean($string)
{
    return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
}

function checkWord($data){
    if (!preg_match("/^[a-zA-Z ]*$/",$data)) { 
    
      throw new Exception("Only letters and white space allowed");
    }
    
    return true;
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

function get_word(){
  echo $word;
}


?>