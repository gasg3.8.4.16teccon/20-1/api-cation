# Inglés API-Cation

La aplicación Inglés API-cation tiene como objetivo principal la traducción de palabras en inglés al idioma español. Se mostrarán ejemplos de oraciones de uso de la palabra; además, si la palabra a traducir es un verbo, se mostrará las diferentes conjugaciones del mismo. 

La aplicación entiende que el idioma inglés no se traduce de forma literal, por lo que muestra diferentes estilos y ejemplos de traducciones. Estos estilos son consumidos por otras API realizadas por terceros. 

Ingles API-cation, muestra una interfaz amigable con el usuario, para que sea sencillo de usar y muy útil; cumpliendo con todas las expectativas del usuario final.


## Iniciando

### Pre-requisitos

* XAMMP
* PHP 7.x
* Mysql 

## Instalación

Las siguientes indicaciones serán para poder instalar Ingles API-Cation de manera local. 

* Tener instalado de manera correcta XAMPP en su computadora. 
* Descargamos o clonamos el repositorio de la aplicación.
* Utilizaremos la última versión de Ingles API-Cation, hasta la fecha es la V3.
* Copiamos la carpeta inglesApiCationV3 en la siguiente ruta: 
```
                                    "rutaxammp/www/inglesApiCationV3"
```
* Una vez copiada la carpeta, podemos probar la aplicación en el navegador web de tu preferencia de la siguiente forma:
```
                                    "http://localhost/inglesApiCationV3/"
```

De igual manera se inserta un video explicando su instalación del software de manera local. En link es el siguiente: www.youtube.com



## Autores

* **Diego Añazco** - *Estudiante de Ing. Software - Universidad La Salle* 
* **María Pinto** - *Estudiante de Ing. Software - Universidad La Salle* 
* **Luis Flores** - *Estudiante de Ing. Software - Universidad La Salle* 
* **Ángel Astorga** - *Profesor de Ing. Software - Universidad La Salle* 

## Licencia

Este proyecto esta licenciado por...

## Versiones de la interfaz del proyecto

#### En las siguientes imagenes se pueden ver las versiones de la interfaz del sistema Inglés API-Cation

### Primera versión de la interfaz del sistema

<a href="https://imgur.com/0YiZ6Gm"><img src="https://i.imgur.com/0YiZ6Gm.jpg" title="source: imgur.com" /></a>

### Segunda versión de la interfaz del sistema

<a href="https://imgur.com/6htjXbm"><img src="https://i.imgur.com/6htjXbm.png" title="source: imgur.com" /></a>

### Tercera versión de la interfaz del sistema

<a href="https://imgur.com/LtERvCw"><img src="https://i.imgur.com/LtERvCw.png" title="source: imgur.com" /></a>

## Api del Proyecto

### Estos son los pasos para poder probar el API de Inglés APICation 

 * Paso 1 - Login: Este método retorna el valor del token necesario para poder consultar a la API. La ruta es la siguiente:

    http://35.193.220.168/~lfloresr/APIproyecto/loggin.php

    Es necesario ingresar los siguientes headers:

    id : 1

    password : password
 * Paso 2 - Se realizan las consultas a la API. Aquí también se requiere ingresar los headers. 

    id: 1

    token: token 

    TranslateR

    ■	http://35.193.220.168/~lfloresr/APIproyecto/inglesapi.php?fuente=translatR&palabra=run
    RapidAPi: Conjugaciones

    ■	Indicativo: http://35.193.220.168/~lfloresr/APIproyecto/inglesapi.php?fuente=rapidApi&conjugacion=indicative&palabra=run

    ■	Condicional: http://35.193.220.168/~lfloresr/APIproyecto/inglesapi.php?fuente=rapidApi&conjugacion=conditional&palabra=run
    
    ■	Pasivo: http://35.193.220.168/~lfloresr/APIproyecto/inglesapi.php?fuente=rapidApi&conjugacion=conditional&palabra=run

Nota: los enlaces que se indicaron son ejemplos. Para cambiar la búsqueda, cambie el valor de “run” por la palabra que desee consultar

## Herramientas

### Las herramientas que se utilizaron para el desarrollo del sistema Inglés APICation

* Php 7.x
* Css 3
* Bootstrap 3
* Js
* XAMMP

## Agradecimientos

* Agredecemos a la Universidad La Salle - Arequipa, por brindarnos la oportunidad de diseñar la aplicación.
